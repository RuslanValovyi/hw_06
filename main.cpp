#include <iostream>
#include "array.h"

int main() {
	Array* pa = new Array(5,10); // new object
	Array* pa1 = new Array(5,10); // new object

	// --------------------------------------- Check: is pa->matrix equal to pa1->matrix
	std::cout << "Matrix pa:" << std::endl;
	pa->printMatrix();

	std::cout << "Matrix pa1:" << std::endl;
	pa1->printMatrix();

	if (pa->equal(*pa1)) {
		std::cout << "pa->matrix is equal to pa1->matrix\n" << std::endl;
	}
	else {
		std::cout << "pa->matrix isn't equal to pa1->matrix\n" << std::endl;
	}

	// --------------------------------------- Test fillData
	pa1->fillData();

	std::cout << "Matrix pa1 (after fillData):" << std::endl;
	pa1->printMatrix();

	if (pa->equal(*pa1)) {
		std::cout << "pa->matrix is equal to pa1->matrix\n" << std::endl;
	}
	else {
		std::cout << "pa->matrix isn't equal to pa1->matrix\n" << std::endl;
	}

	// --------------------------------------- Test transpose matrix
	Array* pb = pa->transpose(); // new object

	std::cout << "Matrix pb (transpose from pa):" << std::endl;
	pb->printMatrix();

	pa->add(*pa1);

	std::cout << "Matrix pa (after add(*pa1)):" << std::endl;
	pa->printMatrix();

	pa->subtract(*pa1);

	std::cout << "Matrix pa (after subtract(*pa1)):" << std::endl;
	pa->printMatrix();

	delete pa;
	delete pa1;
	delete pb;

	return 0;
}
