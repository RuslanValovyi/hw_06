#pragma once

class Array {
public:

    explicit Array(int dmns=5, int sz=5)
	: size(sz)
	, dimensions(dmns) {
		std::cout << "Custom constructor of Array " << dimensions << "x" << size << std::endl;
		matrix = new int*[dimensions];
		for (int i = 0; i < dimensions; i++) matrix[i] = new int[size];
		for (int i = 0; i < dimensions; i++) {
			for (int j = 0; j < size; j++) {
				matrix[i][j] = i*10 + j;
			}
		}
	}

	~Array() {
		std::cout << "Array is being destroyed" << std::endl;
		for (int i = 0; i < dimensions; i++) delete[]matrix[i];
		delete[]matrix;
	}

    void fillData() {
		srand(time(0));
		for (int i = 0; i < dimensions; i++) {
			for (int j = 0; j < size; j++) {
				matrix[i][j] = rand() % 10;
			}
		}
	}

    bool equal(const Array& a) {
		bool res = true;
		if (size != a.size || dimensions != a.dimensions) {
			res = false;
		}
		for (int i = 0; i < dimensions; i++) {
			if (res != true) {
				break;
			}
			for (int j = 0; j < size; j++) {
				if (matrix[i][j] != a.matrix[i][j]) {
					res = false;
					break;
				}
			}
		}
		return res;
	}

    int printMatrix() {
		for (int i = 0; i < dimensions; i++) {
			for (int j = 0; j < size; j++) {
				std::cout << matrix[i][j] << " ";
			}
			std::cout << std::endl;
		}
		std::cout << std::endl;
	}

	Array* transpose () {
		Array* a_res = new Array(size, dimensions);
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < dimensions; j++) {
				a_res->matrix[i][j] = matrix[j][i];
			}
		}
		return a_res;
	}

    bool add(const Array& a) {
		bool res = true;
		if (size != a.size || dimensions != a.dimensions) {
			res = false;
		}
		for (int i = 0; i < dimensions; i++) {
			for (int j = 0; j < size; j++) {
				matrix[i][j] += a.matrix[i][j];
			}
		}
		return res;
	}

    bool subtract(const Array& a) {
		bool res = true;
		if (size != a.size || dimensions != a.dimensions) {
			res = false;
		}
		for (int i = 0; i < dimensions; i++) {
			for (int j = 0; j < size; j++) {
				matrix[i][j] -= a.matrix[i][j];
			}
		}
		return res;
	}

private:
    const int size;
    const int dimensions;
	int **matrix;
};
