#include <iostream>
#include "make_double.h"

namespace ns1 {

int makeDouble(int a)
{
	return a*2;
}

void printName()
{
	std::cout << "ns1: Use library 'make_double'." << std::endl;
}

}
