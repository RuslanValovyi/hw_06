#ifndef PRINTSTRING_H
#define PRINTSTRING_H

namespace ns2 {
	void printString(const std::string&);
	void printName();
}

#endif//PRINTSTRING_H
