#include <iostream>
#include "print_string.h"

namespace ns2 {

void printString(const std::string&s)
{
	std::cout << s << std::endl;
}

void printName()
{
	std::cout << "ns2: Use library 'print_string'." << std::endl;
}

}
